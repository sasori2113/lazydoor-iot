#include <ESP8266WiFi.h>
#include <Servo.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
MDNSResponder mdns;

const char* ssid = "ASA-Hotspot";            //Variable
const char* password = "nganunganu";
const int led = LED_BUILTIN;

int UP = 16;      //PORT
int DOWN = 5;
int ledup = 4;
int leddown = 0;
int ledlock = 2;
int ledunlock = 14;
int buzzer = 12;
//int stanby = D13;

ESP8266WebServer server(80);         //deklarasi web server
Servo lock;                          //deklarasi servo
String webPage = "";                 //memanggil halaman web page

void setup(void) {
    lock.attach(13);                 //Port servo / lock
 
webPage += "<body bgcolor=#1e1e1d >";                                                                                                               //Isi halaman web

webPage += "<br><br><br><br><center><b><h1><font  color = white size =40> LAZY GARAGE is Ready</font></b></center></br></br></br></br>";

webPage +="<br><br></br></br>";
webPage +="<center><a href=\"/UP\"><img src = http://188.166.218.209/wemos/up.png width=500 height=160></img></font></a></center>";
webPage += "<center><a href=\"/DOWN\"><br><img src =http://188.166.218.209/wemos/down.png width=500 height=160></img></a></center>";

webPage += "<center><a href=\"/LOCK\"><br><img src = http://188.166.218.209/wemos/lock.png width=500 height=160></img></font></a></center>";
webPage += "<center><a href=\"/UNLOCK\"><br><img src = http://188.166.218.209/wemos/unlock.png width=500 height=160></img></font></a></center>";
pinMode(UP, OUTPUT);              //deklarasi jika pin berada pada output
digitalWrite(UP, HIGH);           //untuk membaca keadaan 
pinMode(DOWN, OUTPUT);            
digitalWrite(DOWN, HIGH);         
pinMode(buzzer, OUTPUT);          
digitalWrite(buzzer, LOW);
pinMode(ledup, OUTPUT);
digitalWrite(ledup, LOW);
pinMode(leddown, OUTPUT);
digitalWrite(leddown, LOW);
//pinMode(stanby, OUTPUT);
//digitalWrite(stanby, LOW);
pinMode(ledlock, OUTPUT);
digitalWrite(ledlock, LOW);
pinMode(ledunlock, OUTPUT);
digitalWrite(ledunlock, LOW);


Serial.begin(115200);             //untuk mensingkronasikan pc dengan alat / wmos
WiFi.begin(ssid, password);       //untuk menampilkan ssid dan password diserial monitor arduino
Serial.println("");               //output
delay(500);

while (WiFi.status() != WL_CONNECTED) {
digitalWrite(led, 1);      //status lampu wifi hidup jika terkoneksi
delay(100);
digitalWrite(led, 0);      //status lampu wifi mati
}
Serial.println("");
Serial.print("Connected to ");
Serial.println(ssid);
Serial.print("IP address: ");
Serial.println(WiFi.localIP());     //menampilkan ip address wemos
if (mdns.begin("esp8266", WiFi.localIP())) {      //untuk memanggil ip wemos di web browser
}
server.on("/", []() {                             //untuk memanggil fungsi server untuk mengaktifkan port 80
server.send(200, "text/html", webPage);           //menampilkan fungsi webpage
});
server.on("/UP", []() {                           //memanggil fungsi up(pintu terbuka)
server.send(200, "text/html",webPage);
digitalWrite(leddown, LOW);                       //jika tombol up dipencet maka lampu down mati
  tone(buzzer, 1300);                             //buzzer berbunyi
  digitalWrite(ledup, HIGH);                      //lampu up akan berkedip dan buzzer akan mengeluarkan suara secara berkala
  delay(500);        
  noTone(buzzer);
  digitalWrite(ledup, LOW);   
  delay(500); 
  tone(buzzer, 1300); 
  digitalWrite(ledup, HIGH);
  delay(500);       
  noTone(buzzer);   
  digitalWrite(ledup, LOW);
  delay(500);
  tone(buzzer, 1300);
  digitalWrite(ledup, HIGH); 
  delay(500);        
  noTone(buzzer);
  digitalWrite(ledup, LOW);   
  delay(500); 
      
digitalWrite(UP, LOW);                            //motor akan hidup dan menarik gerbang ke atas
tone(buzzer, 1300);
digitalWrite(ledup, HIGH);
delay(500);       
noTone(buzzer);   
digitalWrite(ledup, LOW);
delay(500); 
 
  digitalWrite(UP, HIGH);                         //seperti yang diatas tetapi lebih cepat
  tone(buzzer, 1300);
  digitalWrite(ledup, HIGH); 
  delay(200);        
  noTone(buzzer);
  digitalWrite(ledup, LOW);   
  delay(200); 
  tone(buzzer, 1300); 
  digitalWrite(ledup, HIGH);
  delay(200);       
  noTone(buzzer);   
  digitalWrite(ledup, LOW);
  delay(200);
  tone(buzzer, 1300);
  digitalWrite(ledup, HIGH); 
  delay(200);        
  noTone(buzzer);
  digitalWrite(ledup, LOW);   
  delay(200); 
  tone(buzzer, 1300); 
  digitalWrite(ledup, HIGH);
  delay(200);       
  noTone(buzzer);   
  digitalWrite(ledup, LOW);
  delay(200);
  digitalWrite(ledup, HIGH);
});
server.on("/DOWN", []() {
server.send(200, "text/html", webPage);                       //seperti langkah langkah yang di atas tetapi untuk yang kebawah
digitalWrite(ledup, LOW);
  tone(buzzer, 1300);
  digitalWrite(leddown, HIGH); 
  delay(500);        
  noTone(buzzer);
  digitalWrite(leddown, LOW);   
  delay(500); 
  tone(buzzer, 1300); 
  digitalWrite(leddown, HIGH);
  delay(500);       
  noTone(buzzer);   
  digitalWrite(leddown, LOW);
  delay(500);
  tone(buzzer, 1300);
  digitalWrite(leddown, HIGH); 
  delay(500);        
  noTone(buzzer);
  digitalWrite(leddown, LOW);   
  delay(500); 
    
digitalWrite(DOWN, LOW);
tone(buzzer, 1300);
digitalWrite(leddown, HIGH);
delay(500);       
noTone(buzzer);   
digitalWrite(leddown, LOW);
delay(500); 
 
digitalWrite(DOWN, HIGH);
tone(buzzer, 1300);
  digitalWrite(leddown, HIGH); 
  delay(200);        
  noTone(buzzer);
  digitalWrite(leddown, LOW);   
  delay(200); 
  tone(buzzer, 1300); 
  digitalWrite(leddown, HIGH);
  delay(200);       
  noTone(buzzer);   
  digitalWrite(leddown, LOW);
  delay(200);
  tone(buzzer, 1300);
  digitalWrite(leddown, HIGH); 
  delay(200);        
  noTone(buzzer);
  digitalWrite(leddown, LOW);   
  delay(200); 
  tone(buzzer, 1300); 
  digitalWrite(leddown, HIGH);
  delay(200);       
  noTone(buzzer);   
  digitalWrite(leddown, LOW);
  delay(200);
  digitalWrite(leddown, HIGH);
});
server.on("/LOCK", []() {                                   //memanggil fungsi lock
  server.send(200, "text/html", webPage);
 digitalWrite(ledunlock, LOW);                              //led lock berkedip dan buzzer mengeluarkan bunyi berjeda
   lock.write(180);              
  delay(10);
  tone(buzzer, 1300);
  digitalWrite(ledlock, HIGH); 
  delay(60);        
  noTone(buzzer);
  digitalWrite(ledlock, LOW);   
  delay(60); 
  tone(buzzer, 1300); 
  digitalWrite(ledlock, HIGH);
  delay(60);       
  noTone(buzzer);   
  digitalWrite(ledlock, LOW);
  delay(60); 
  digitalWrite(ledlock, HIGH);        
 });
server.on("/UNLOCK", []() {
server.send(200, "text/html", webPage);
digitalWrite(ledlock, LOW);
  lock.write(85);              
  delay(10);  
  tone(buzzer, 1300);
  digitalWrite(ledunlock, HIGH); 
  delay(60);        
  noTone(buzzer);
  digitalWrite(ledunlock, LOW);   
  delay(60); 
  tone(buzzer, 1300); 
  digitalWrite(ledunlock, HIGH);
  delay(60);       
  noTone(buzzer);   
  digitalWrite(ledunlock, LOW);
  delay(60);
  digitalWrite(ledunlock, HIGH);                     
});   
server.begin();                                   //penanda pada serial monitor jika wemos sudah siap
Serial.println("Server is Ready");
}

void loop(void) {                                 //berguna untuk melaksanakan / mengeksekusi perintah program yang telah dibuat

server.handleClient(); 
}


